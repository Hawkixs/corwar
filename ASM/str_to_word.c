/*
** str_to_word.c for  in /home/sauter_t/rendu/corewar/corwar
** 
** Made by sauter_t
** Login   <sauter_t@epitech.net>
** 
** Started on  Tue Feb  4 00:11:18 2014 sauter_t
** Last update Tue Feb  4 01:33:59 2014 sauter_t
*/

#include <stdlib.h>

int	count_tab(char *str, char *delim)
{
  int	i;
  int	count;

  i = 0;
  count = 0;
  while (str[i] != 0)
    {
      if (is_in(str[i], delim) && !is_in(str[i + 1], delim))
	count++;
      i++;
    }
  return (count);
}

void	change_tab(char **tab, char *str, char *delim)
{
  int	i;
  int	j;

  i = 0;
  j = 0;
  tab[j++] = str;
  while (str[i] != 0)
    {
      if (is_in(str[i], delim) && !is_in(str[i + 1], delim))
	{
	  str[i++] = 0;
	  tab[j++] = &str[i];
	}
      else if (is_in(str[i], delim) && is_in(str[i + 1], delim))
	str[i++] = 0;
      else
	i++;
    }
  tab[j] = NULL;
}

char	**str_to_word(char *str, char *delim)
{
  int	nb;
  char	**tab;

  nb = count_tab(str, delim);
  if ((tab = malloc((nb + 2) * sizeof(char*))) == NULL)
    return (NULL);
  change_tab(tab, str, delim);
  return (tab);
}
