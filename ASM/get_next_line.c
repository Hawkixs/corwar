
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

char    *my_strcpy(char *s1, char *s2, int n)
{
  int   i;

  i = 0;
  while (s2[i])
    s1[n++] = s2[i++];
  s1[n] = '\0';
  return (s1);
}

char    *my_realloc(char *str, size_t len)
{
  char  *new;
  int   i;

  i = 0;
  if ((new = malloc(len)) == NULL)
    return (NULL);
  new = my_strcpy(new, str, i);
  free(str);
  return (new);
}

int	check_end_line(char *bufftmp)
{
  int	i;

  i = 0;
  while (bufftmp[i] != '\n' && bufftmp[i] != '\0')
    i++;
  if (bufftmp[i] == '\n')
    return (0);
  else
    return (1);
}

char	*cpy_buff(char *str, char *bufftmp, int cpt)
{
  int	i;
  int	j;

  i = 0;
  j = 0;
  if (cpt == 0)
    {
      if (((str = malloc(sizeof(*str) * 513)) == NULL))
	return (NULL);
      while (bufftmp[i])
	str[i] = bufftmp[i++];
      str[i] = '\0';
    }
  else
    {
      if ((str = my_realloc(str, (514 * (cpt + 1)) * sizeof(*str))) == NULL)
        return (NULL);
      i = 512 * cpt;
      while (bufftmp[j])
	str[i++] = bufftmp[j++];
      str[i] = '\0';
    }
  return (str);
}

char 	*save_line(char *str, char *bufftmp, char *buffer, int cpt)
{
  int   i;
  int   j;
  int	k;

  k = 0;
  i = 0;
  j = 0;
  if (cpt == 0 || str == NULL)
    {
      if (((str = malloc(sizeof(*str) * 513)) == NULL))
        return (NULL);
      while (bufftmp[i] != '\n' && bufftmp[i])
        str[i] = bufftmp[i++];
      str[i] = '\0';
      i++;
      while (bufftmp[i])
	buffer[j++] = bufftmp[i++];
      buffer[j] = '\0';
    }
  else
    {
      if ((str = my_realloc(str, (512 * (cpt + 1)) * sizeof(*str))) == NULL)
        return (NULL);
      i = 512 * cpt;
      while (bufftmp[j] != '\n' && bufftmp[j])
        str[i++] = bufftmp[j++];
      str[i] = '\0';
      j++;
      while (bufftmp[j])
        buffer[k++] = bufftmp[j++];
      buffer[k] = '\0';
    }
  return (str);
}

int	check_end(char *buffer)
{
  int	i;

  i = 0;
  while (buffer[i])
    {
      if (buffer[i] == '\n')
	return (1);
      i++;
    }
  buffer[0] = '\0';
  return (0);
}

char	*get_save_line(char *str, char *buffer, int fd)
{
  int	i;
  char	tmp[512];
  int	j;
  int	size;

  j = 0;
  i = 0;
  if (((str = malloc(sizeof(*str) * 1024)) == NULL))
    return (NULL);
  while (buffer[i] != '\n' && buffer[i])
    str[i] = buffer[i++];
  str[i] = '\0';
  if (buffer[i] != '\0')
    i++;
  while (buffer[i])
    tmp[j++] = buffer[i++];
  tmp[j] = '\0';
  if ((check_end(buffer)) == 0)
    {
      if ((size = read(fd, buffer, 512)) < 1)
        return (NULL);
      j = 0;
      while (buffer[j] != '\n' && buffer[j])
	str[i++] = buffer[j++];
      str[i] = '\0';
      i = 0;
      j++;
      while (buffer[j])
	tmp[i++] = buffer[j++];
      tmp[i] = '\0';
      i = 0;
      j = 0;
      while (tmp[j])
	buffer[i++] = tmp[j++];
      buffer[i] = '\0';
      return (str);
    }
  i = 0;
  j = 0;
  while (tmp[j])
    buffer[i++] = tmp[j++];
  buffer[i] = '\0';
  return (str);
}

char	*get_next_line(int fd)
{
  static char buffer[512];
  char	*str;
  char	bufftmp[512];
  int	size;
  int	cpt;

  cpt = 0;
  if (buffer[0] == '\0')
    {
      if ((size = read(fd, bufftmp, 512)) < 1)
	return (NULL);
      bufftmp[size] = '\0';
      while ((check_end_line(bufftmp)) != 0)
	{
	  if ((str = cpy_buff(str, bufftmp, cpt++)) == NULL)
	    return (NULL);
	  if ((size = read(fd, bufftmp, 512)) < 1)
	    return (NULL);
	  bufftmp[size] = '\0';
	}
      str = save_line(str, bufftmp, buffer, cpt);
    }
  else
    str = get_save_line(str, buffer, fd);
  return (str);
}
/*
int	main(int ac, char **av)
{
  char *str;
  int	fd;

  if (ac > 1)
    {
      fd = open(av[1], O_RDONLY);
      while ((str = get_next_line(fd)) != NULL)
	{
	  printf("%s\n", str);
	  free(str);
	}
    }
  return (0);
}
*/
