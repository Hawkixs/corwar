/*
** lib2.c for  in /home/sauter_t/rendu/corewar/corwar
** 
** Made by sauter_t
** Login   <sauter_t@epitech.net>
** 
** Started on  Tue Mar 18 20:40:45 2014 sauter_t
** Last update Tue Mar 25 16:05:14 2014 Kevin LOISELEUR
*/

#include <stdlib.h>

char	*my_strcut(char *str, char del)
{
  int	i;

  i = 0;
  while (str[i] != del || str[i] != 0)
    i++;
  if (str[i] == del)
    str[i] = 0;
}

char	*my_strdup(char *str)
{
  char	*buf;
  int	i;

  i = 0;
  if ((buf = malloc((my_strlen(str) + 1) * sizeof(char))) == NULL)
    return (NULL);
  while (str[i] != 0)
    buf[i] = str[i++];
  return (buf);
}

int	my_open(const char *pathname, int flags)
{
  int	fd;

  fd = open(pathname, flags);
  if (fd == -1)
    {
      printf("Error on opening the file\n");
      exit(ERROR);
    }
  return (fd);
}
