/*
** put_in_lab.c for  in /home/sauter_t/rendu/corewar/corwar
** 
** Made by sauter_t
** Login   <sauter_t@epitech.net>
** 
** Started on  Tue Mar 18 20:11:05 2014 sauter_t
** Last update Wed Mar 26 13:16:20 2014 Kevin LOISELEUR
*/

#include <stdlib.h>
#include <stdbool.h>
#include "asm.h"
#include "op.h"

static bool	is_label(char *str)
{
  int	i;

  i = 0;
  while (str[i + 1] != 0)
    i++;
  if (str[i] == LABEL_CHAR)
    return (true);
  return (false);
}

void	get_name(char **tab, t_name *name)
{
  if (my_strcmp(tab[0], NAME_CMD_STRING) == 0)
    name->name = &tab[1];
  if (my_strcmp(tab[0], COMMENT_CMD_STRING) == 0)
    name->comment = &tab[1];
}

static t_lab	*create_label(t_lab *label, char *name)
{
  t_lab *elem;

  if ((elem = malloc(sizeof(*elem))) == NULL)
    return (NULL);
  if (name == NULL)
    name = my_strdup("first\0");
  elem->name = name;
  elem->next = NULL;
  elem->lines = NULL;
  elem->prev = label;
  if (label != NULL)
    label->next = elem;
  label = elem;
  return (elem);
}

static t_cmd	*my_put_in_cmd(t_lab *label, char **tab, char cmd)
{
  t_cmd	*elem;
  t_cmd	*tmp;

  if ((elem = malloc(sizeof(*elem))) == NULL)
    return (NULL);
  elem->cmd = cmd;
  elem->args = &tab[1];
  tmp = label->lines;
  elem->next = NULL;
  if (tmp == NULL)
    {
      label->lines = elem;
      return;
    }
  while (tmp->next != NULL)
    tmp = tmp->next;
  tmp->next = elem;
  elem->prev = tmp;
}

t_lab	*dispatch_lab(char **tab, t_lab *label)
{
  char	cmd;

  if (is_label(tab[0]) == true)
    {
      if ((label = create_label(label, tab[0])) == NULL)
	return (NULL);
      if (tab[1] != NULL)
	if ((cmd = check_cmd(&tab[1])) >= 0)
	  if ((my_put_in_cmd(label, &tab[1], cmd)) == NULL)
	    return (NULL);
    }
  else
    {
      if (label == NULL)
	{
	  if ((label = create_label(label, NULL)) == NULL)
	    return (NULL);
	}
      if ((cmd = check_cmd(tab)) >= 0)
	if (my_put_in_cmd(label, tab, cmd) == NULL)
	  return (NULL);
    }
  return (label);
}
