/*
** check_cmd.c for check_cmd in /home/delafo_c/travail/corwar/corwar
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Tue Mar 25 16:35:45 2014 delafo_c
** Last update Wed Apr  2 18:16:21 2014 delafo_c
*/

#include <stdio.h>
#include <stdlib.h>
#include "op.h"

int		check_instruction(char *cmd)
{
  int		i;
  int		check;
  int		yes;

  i = 0;
  while ((op_tab[i].mnemonique) || !yes)
    {
      // check si label ou non
      if ((strncmp(cmd, op_tab[i].mnemonique,
		      my_strlen(op_tab[i].mnemonique))) == 0)
	yes = 1;
      else
	i++;
    }
  if (op_tab[i].mnemonique == 0)
    {
      printf("Instruction not found in the line : \n '%s'", cmd);
      return (-1);
    }
  return (i);
}

int		check_label(char *cmd, char **label)
{
  int		i;

  i = 0;
  /*
  ** return 1 if label is not in cmd
  ** check if label contains only LABEL_CHARS
  ** and check if label exists (else return -1)
  */
  return (0);
}

int		check_nbr_args(char *cmd, char nbr_args)
{
  int		i;
  char		nbr_sep;

  i = 0;
  nbr_sep = 1;
  while (cmd[i] != '\0')
    {
      if (cmd[i] == SEPARATOR_CHAR)
	nbr_sep++;
      i++;
    }
  if (nbr_sep != nbr_args)
    {
      printf("Wrong nbr args!\nNb args is %d but should be", nbr_sep);
      printf("%d\nAt line :'%s'.\n", nbr_args, cmd);
      return (-1);
    }
  return (0);
}

int		check_type(char *cmd, args_type_t type[MAX_ARGS_NUMBER])
{
  return (0);
}

int		check_cmd(char *cmd)
{
  int		i;
  int		check;

  i = 0;
  if ((i = check_instruction(cmd)) == -1)
    return (-1);
  if (check_nbr_args(cmd, op_tab[i].nbr_args) == -1)
    return (-1);
  if (check_type(cmd, op_tab[i].type) == -1)
    return (-1);
  return (0);
}

int		main(int ac, char **av)
{
  if (ac != 2)
    return (-1);
  if (check_cmd(av[1]) == 0)
    printf("OK\n");
  return (0);
}
