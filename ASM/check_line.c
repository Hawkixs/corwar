/*
** check_line.c for  in /home/sauter_t/rendu/corewar/corwar
** 
** Made by sauter_t
** Login   <sauter_t@epitech.net>
** 
** Started on  Fri Jan 31 17:58:31 2014 sauter_t
** Last update Wed Apr  2 17:24:53 2014 Kevin LOISELEUR
*/

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "op.h"
#include "asm.h"


static int	check_space(char c)
{
  if (c == ' ' || c == '\t')
    return (1);
  else
    return (0);
}

/*
** clean all the space and '\t'
*/

static char	*clean_line(char *str)
{
  int	i;
  int	j;
  char	*buf;

  i = 0;
  j = 0;
  if ((buf = malloc((my_strlen(str) + 1) * sizeof(char))) == NULL)
    return (NULL);
  while ((str[i] == ' ' || str[i] == '\t') && str[i] != 0)
    i++;
  while (str[i] != 0)
    {
      if (check_space(str[i]) && check_space(str[i + 1]))
	i = i;
      else if (str[i] == '\t' || str[i] == ' ')
	buf[j++] = ' ';
      else
	buf[j++] = str[i];
      i++;
    }
  j--;
  while (buf[j] == ' ')
    j--;
  buf[++j] = 0;
  return (buf);
}

/*
** check empty strings
*/

static int	check_line_space(char *str)
{
  int	i;

  i = 0;
  while ((str[i] == ' ' || str[i] == '\t') && str[i] != 0)
    i++;
  if (str[i] == 0)
    return (0);
  else
    return (1);
}

/*
** check out comments / remote
*/

static char	*check_line(char *str)
{
  char	*buf;
  int	i;

  i = 0;
  if ((buf = clean_line(str)) == NULL)
    return (NULL);
  else
    {
      if (check_line_space(str) == 0)
	{
	  free(str);
	  return (buf);
	}
      free(str);
      buf = realloc(buf, my_strlen(buf));
    }
  while (buf[i] != 0 && buf[i] != COMMENT_CHAR)
    i++;
  if (buf[i] == COMMENT_CHAR)
    buf[i - 1] = 0;
  return (buf);
}

/*
** send the char *, return a char ** with a check
** return an empty string if the original string contains only space or '\t'
** take care to the label
*/

static char	**check_error(char *str)
{
  char		*buf;
  char		delim[2];
  char		**tab;

  delim[0] = ' ';
  delim[1] = SEPARATOR_CHAR;
  if ((buf = check_line(str)) == NULL)
    return (NULL);
  if ((tab = str_to_word(buf, delim)) == NULL)
    return (NULL);
  return (tab);
}

t_lab		*get_file(int fd, char **tab)
{
  char		*str;
  t_name	*name;
  t_lab		*label;

  label = NULL;
  if ((name = malloc(sizeof(*name))) == NULL)
    return (NULL);
  while ((str = get_next_line(fd)) != NULL)
    {
      if ((tab = check_error(str)) == NULL)
	return (NULL);
      if (my_strcmp(tab[0], NAME_CMD_STRING)  == 0 ||
      	  my_strcmp(tab[0], COMMENT_CMD_STRING) == 0)
      	get_name(tab, name);
      else if (my_strcmp(tab[0], "") != 0)
      	label = dispatch_lab(tab, label);
    }
  while (label->prev != NULL)
    label = label->prev;
  return (label);
}

int	main(int ac, char **av)
{
  char	**tab;
  int	fd;
  t_lab	*label;

  if (ac != 2)
    my_putstr("Usage: ./a.out [champ.s]");
  if ((fd = open(av[1], O_RDONLY)) == -1)
    return (-1);
  if ((label = get_file(fd, tab)) == false)
    return (-1);
  close(fd);
  while (label != NULL)
    {
      printf("%s\n", label->name);
      while (label->lines != NULL)
  	{
  	  printf("%d: %s\n", label->lines->cmd, label->lines->args[0]);
  	  label->lines = label->lines->next;
  	}
      label = label->next;
    }
}
