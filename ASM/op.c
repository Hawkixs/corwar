/*
** op.c for  korewar
**
** Made by Astek
** Login   <astek@epitech.net>
**
** Started on  Mon Mar 30 11:14:31 2009 Astek
** Last update Wed Mar 19 16:42:37 2014 sauter_t
*/

/*
** Ne pas passer ce fichier dans la moulinette de Norme.
*/

#include "op.h"
#include <stdio.h>
#include <stdlib.h>

op_t    op_tab[] =
  {
    {"live", 1, {T_DIR}, 1, 10, "alive"},
    {"ld", 2, {T_DIR | T_IND, T_REG}, 2, 5, "load"},
    {"st", 2, {T_REG, T_IND | T_REG}, 3, 5, "store"},
    {"add", 3, {T_REG, T_REG, T_REG}, 4, 10, "addition"},
    {"sub", 3, {T_REG, T_REG, T_REG}, 5, 10, "soustraction"},
    {"and", 3, {T_REG | T_DIR | T_IND, T_REG | T_IND | T_DIR, T_REG}, 6, 6,
     "et (and  r1, r2, r3   r1&r2 -> r3"},
    {"or", 3, {T_REG | T_IND | T_DIR, T_REG | T_IND | T_DIR, T_REG}, 7, 6,
     "ou  (or   r1, r2, r3   r1 | r2 -> r3"},
    {"xor", 3, {T_REG | T_IND | T_DIR, T_REG | T_IND | T_DIR, T_REG}, 8, 6,
     "ou (xor  r1, r2, r3   r1^r2 -> r3"},
    {"zjmp", 1, {T_DIR}, 9, 20, "jump if zero"},
    {"ldi", 3, {T_REG | T_DIR | T_IND, T_DIR | T_REG, T_REG}, 10, 25,
     "load index"},
    {"sti", 3, {T_REG, T_REG | T_DIR | T_IND, T_DIR | T_REG}, 11, 25,
     "store index"},
    {"fork", 1, {T_DIR}, 12, 800, "fork"},
    {"lld", 2, {T_DIR | T_IND, T_REG}, 13, 10, "long load"},
    {"lldi", 3, {T_REG | T_DIR | T_IND, T_DIR | T_REG, T_REG}, 14, 50,
     "long load index"},
    {"lfork", 1, {T_DIR}, 15, 1000, "long fork"},
    {"aff", 1, {T_REG}, 16, 2, "aff"},
    {0, 0, {0}, 0, 0, 0}
  };

/*
** check cmd and nbrs_args, put an error message if there is a problem
** return -1 if there isn't the good nbr of arguments
** return -2 if (the command doesn't exist
*/

char	check_cmd(char **tab)
{
  int	i;
  int	j;

  i = 0;
  j = 0;
  while (op_tab[i].mnemonique != 0)
    {
      if (my_strcmp(tab[j], op_tab[i].mnemonique) == 0)
	{
	  while (tab[j] != NULL)
	    j++;
	  if (op_tab[i].nbr_args == j - 1)
	    return (op_tab[i].code);
	  else
	    {
	      my_putstr(" wrong number of arguments.\n");
	      return (-1);
	    }
	  j = 0;
	}
      i++;
    }
  my_putstr(tab[0]);
  my_putstr(": this command doesn't exist.\n");
  return (-2);
}
