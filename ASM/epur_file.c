/*
** epur_file.c for epur file in /home/loisel_k/Taff/C/C-Prog-Elem/B2/Corewar/corwar
** 
** Made by Kevin LOISELEUR
** Login   <loisel_k@epitech.net>
** 
** Started on  Tue Mar 25 16:11:06 2014 Kevin LOISELEUR
** Last update Tue Mar 25 16:31:23 2014 Kevin LOISELEUR
*/

#include "asm.h"
#include <stdlib.h>

void		check_comment(char *line)
{
  int		i;

  i = 0;
  while (line && line[i] && line[i] != '#')
    i++;
  if (line[i] == '#')
    line[i] = 0;
  return ;
}

