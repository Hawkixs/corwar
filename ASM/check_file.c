/*
** check_file.c for fct check for file in /home/loisel_k/Taff/C/C-Prog-Elem/B2/Corewar/corwar
** 
** Made by Kevin LOISELEUR
** Login   <loisel_k@epitech.net>
** 
** Started on  Tue Mar 25 16:03:52 2014 Kevin LOISELEUR
** Last update Tue Mar 25 16:08:52 2014 Kevin LOISELEUR
*/

#include "asm.h"

int	check_extension(char *fname)
{
  int	check;
  int	i;

  i = 0;
  check = 0;
  while (fname && fname[i] != '.' && fname[i])
    i++;
  if (fname[i] == 0)
    {
      my_printf("Your champion has no extension.\n Please Check It.\n");
      return (1);
    }
  if (fname[i + 1] != 's' && fname[i + 2] != 0)
    {
      my_printf("Your champions has a bad extension.\n Please check It.\n");
      return (1);
    }
  return (0);
}

int	check_next_lab(char *ligne)
{
  if (ligne[0] == '\t')
    return (0);
  else
    return (1);
}


int	check_start(char *ligne)
{
  if (ligne[0] == '.' || ligne[0] == '\0')
    return (1);
  else
    return (0);
}
