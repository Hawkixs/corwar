

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
//#include <stdio.h>
#include "asm.h"

char	*rename(char *fname)
{
  int	i;
  char	*newname;

  i = 0;
  if ((newname = malloc(sizeof(*newname) * my_strlen(fname) + 3)) == NULL)
    return (NULL);
  if (check_extension(fname))
    return (NULL);
  while (fname && fname[i] != '.' && fname[i])
    newname[i] = fname[i++];
  newname[i] = fname[i++];
  newname[i++] = 'c';
  newname[i++] = 'o';
  newname[i++] = 'r';
  newname[i] = '\0';
  return (newname);
}

int	parsing(int fdr, int fdw)
{
  char *ligne;
  t_lab	*list_lab;
  t_cmd *list_ligne;

  list_lab = NULL;
  list_ligne = NULL;
  while ((ligne = get_next_line(fdr)) != NULL)
    {
      if ((check_start(ligne)) == 0)
	{
	  if ((check_next_lab(ligne)) == 0)
	    my_put_in_list(ligne, &list_ligne);
	  else
	    {
	      my_put_in_list2(list_ligne, &list_lab);
	      list_ligne = NULL;
	      my_put_in_list(ligne, &list_ligne);
	    }
	}
    }
  my_show_list(list_lab);
  return (0);
}

/*
** Verifie Open (evite de surcharger le main
** mais affiche toujours le meme message d'erreur
** si erreur (pb entre fichier a ouvrir et celui ou
** l'on va copier pr message erreur)
*/

int	main(int ac, char **av)
{
  int	fd;
  int	fd2;
  char	*newname;

  if (ac > 1)
    {
      if ((newname = rename(av[1])) == NULL)
	return (0);
      fd = my_open(av[1], (O_RDONLY));
      fd2 = my_open(newname , (O_RDWR | O_CREAT, S_IRWXU | S_IRWXG | S_IRWXO));
      parsing(fd, fd2);
      free(newname);
    }
  return (0);
}

