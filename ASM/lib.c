/*
** lib.c for  in /home/sauter_t/rendu/corewar/corwar
** 
** Made by sauter_t
** Login   <sauter_t@epitech.net>
** 
** Started on  Fri Jan 31 18:30:05 2014 sauter_t
** Last update Tue Feb  4 00:18:52 2014 sauter_t
*/

#include <stdlib.h>
#include <stdio.h>

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (str[i] != 0)
    i++;
  return (i);
}

void	my_putstr(char *str)
{
  write(1, str, my_strlen(str));
}

int	my_strncmp(char *str, char *buf, int nb)
{
  int	i;

  i = 0;
  while (str[i] == buf[i] && nb != 0 && str[i] != 0 && buf[i] != 0)
    {
      nb--;
      i++;
    }
  if (nb != 0)
    return (-1);
  if (str[i - 1] == buf[i - 1])
    return (0);
  return (-1);
}

int	my_strcmp(char *str, char *buf)
{
  int	i;

  i = 0;
  while (str[i] == buf[i] && str[i] != 0 && buf[i] != 0)
    i++;
  if (str[i] == buf[i])
    return (0);
  return (-1);
}

int	is_in(char c, char *delim)
{
  int	i;

  i = 0;
  while (delim[i] != 0)
    {
      if (c == delim[i])
	return (1);
      i++;
    }
  return (0);
}
