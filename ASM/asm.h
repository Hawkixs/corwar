/*
** asm.h for asm in /home/laurio_a/Documents/afs/2re/Corwar/perso/perso
** 
** Made by laurio_a
** Login   <laurio_a@epitech.net>
** 
** Started on  Tue Jan 28 00:39:24 2014 laurio_a
** Last update Tue Mar 25 16:07:27 2014 Kevin LOISELEUR
*/

#ifndef ASM_H_
# define ASM_H_

# define ERROR	1
# define SUCESS	0

typedef struct	s_name
{
  char		**name;
  char		**comment;
}		t_name;

typedef struct	s_lab
{
  char		*name;
  struct s_lab	*prev;
  struct s_lab	*next;
  struct s_cmd	*lines;
}		t_lab;

typedef struct	s_cmd
{
  struct s_cmd	*prev;
  struct s_cmd	*next;
  char		cmd;
  char		**args;
}		t_cmd;

char		*my_strdup(char *str);
char		**str_to_word(char *str, char *delim);
char		*get_next_line(int fd);
int		check_extension(char *fname);
int		check_next_lab(char *ligne);
int		check_start(char *ligne);
int		my_open(const char *pathname, int flags);
t_lab		*dispatch_lab(char **tab, t_lab *label);
void		my_show_list(t_lab *list_lab);

#endif /* !ASM_H_ */
