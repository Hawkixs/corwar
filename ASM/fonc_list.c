/*
** fonc_list_env.c for fonc list env in /home/laurio_a/Documents/afs/2re/shell/test2/test
** 
** Made by laurio_a
** Login   <laurio_a@epitech.net>
** 
** Started on  Sat Jan  4 18:30:40 2014 laurio_a
** Last update Fri Jan 31 19:52:13 2014 Kevin LOISELEUR
*/

#include "asm.h"
#include <stdio.h>
/*
void		ret_deb(t_env **two)
{
  if (*two != NULL)
    {
      while ((*two)->prev != NULL)
	*two = (*two)->prev;
    }
}

void		ret_end(t_env **two)
{
  if (*two != NULL)
    {
      while ((*two)->next != NULL)
	*two = (*two)->next;
    }
}
*/

void    my_show_list(t_lab *list_lab)
{
  if (!list_lab)
    {
      printf("Error: File is empty !\n");
      return ;
    }
  while (list_lab->prev != NULL)
    list_lab = list_lab->prev;
  while (list_lab->next != NULL)
    {
      while(list_lab->list1->prev != NULL)
        list_lab->list1 = list_lab->list1->prev;
      while(list_lab->list1->next != NULL)
        {
          printf("%s\n", list_lab->list1->ligne);
          list_lab->list1 = list_lab->list1->next;
        }
      list_lab = list_lab->next;
    }
  while(list_lab->list1->prev != NULL)
    list_lab->list1 = list_lab->list1->prev;
  while(list_lab->list1->next != NULL)
    {
      printf("%s\n", list_lab->list1->ligne);
      list_lab->list1 = list_lab->list1->next;
    }
  printf("%s\n", list_lab->list1->ligne);
}

int		my_put_in_list(char *nbr, t_ligne **one)
{
  t_ligne		*elem;
  int		tmp;

  if ((elem = malloc(sizeof(*elem))) == NULL)
    return (0);
  if (*one == NULL)
    {
      elem->prev = NULL;
      elem->next = NULL;
      elem->ligne = nbr;
      *one = elem;
    }
  else
    {
      elem->prev = *one;
      elem->next = NULL;
      elem->ligne = nbr;
      (*one)->next = elem;
      *one = elem;
    }
  return (1);
}

int             my_put_in_list2(t_ligne *list, t_lab **one)
{
  t_lab         *elem;
  int           tmp;

  if ((elem = malloc(sizeof(*elem))) == NULL)
    return (0);
  if (*one == NULL)
    {
      elem->prev = NULL;
      elem->next = NULL;
      elem->list1 = list;
      *one = elem;
    }
  else
    {
      elem->prev = *one;
      elem->next = NULL;
      elem->list1 = list;
      (*one)->next = elem;
      *one = elem;
    }
  return (1);
}

/*
** Coder free list en fonction de l'architecture des listes
** Encore a voir.
*/

/*
int		freelist(t_env *two)
{
  t_env		*tmp;

  tmp = two->next;
  while (tmp != NULL)
    {
      tmp = two->next;
      free(two);
      two = tmp;
    }
}
*/
